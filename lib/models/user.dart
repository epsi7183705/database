import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/utils/utils.dart';
import '../services/DatabaseService.dart';
class User {
  final int? id;
  final String firstname;
  final String lastname;
  final String mail;
  static const String table = "users";

  User({
    this.id,
    required this.firstname,
    required this.lastname,
    required this.mail
  });

  // Les informations dans la base de données étant sous la forme "champ":"valeur"
  // cette fonction permet de faire le mapping BDD <-> code
  Map<String, dynamic> toMap() {
    return {
  //    'id': id, // Ne pas spécifier l'id qui sera auti incrémter par sqlite
      'firstname': firstname,
      'lastname': lastname,
      'mail': mail
    };
  }

  factory User.fromMap(Map<String, dynamic> json) => User(
    id: json["id"],
    firstname: json["firstname"],
    lastname: json["lastname"],
    mail: json["mail"]
  );


  String toString() {
    return 'User(id: $id, name: $firstname, lastname: $lastname, mail: $mail)';
  }

  Future<void> insert(User user) async {
    final database = await DatabaseService.db();
    await database.insert(
      table,
      user.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
}
